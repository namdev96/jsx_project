 import React from 'react';
 import ReactDOM from 'react-dom';
// function click(){
    // return 'click on me!';
// }
 const Ap = () => {
     //const buttonText="hello!"   ---we can also use this
     //const buttonText=[10,20];
     //const buttonText=['hi','there']
     const buttonText={text:'click me'}

     return (
    <div>
       <label className="label" htmlFor="name">
       Enter name:
       </label>
       <input id="name"  type="text" />
       <button style={{backgroundColor:"blue",color:"white"}}>
         {buttonText.text}
       </button>
    </div>
);
};
 ReactDOM.render(<Ap />, document.getElementById('root'));
